import { CartDataService } from './cart-data.service';
import { Injectable } from '@angular/core';
import { CartItem } from '../core/interfaces/cart-item.interface';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Product } from '../core/interfaces/product.interface';

interface CartProducts {
    items: CartItem[];
    total: number;
}

@Injectable()
export class CartService {
    protected _products = {
        items: [],
        total: 0,
    };

    protected _cartState = new Subject<CartProducts>();

    constructor(
        protected dataService: CartDataService,
    ) { }

    getStoredCartItems() {
        this.dataService.fetchAll().subscribe(cartItems => {
          this._products.items = cartItems;
          this._products.total = this.calculateTotal(this._products.items);
          this._cartState.next(this._products);
        });
    }


    addProduct(product: Product) {
      const index = this._products.items.findIndex(item => item.id === product.id);

      if (index !== -1) {
        let updatedItem = this._products.items[index];
        updatedItem.amount += 1;
        this._products.items[index] = this.calculateSubtotal(updatedItem);
      } else {
        const newItem = {
          id: product.id,
          amount: 1,
          product: product,
          subtotal: product.price
        };

        this._products.items.push(newItem);
      }

      this._products.total = this.calculateTotal(this._products.items);
      this._cartState.next(this._products);
    }

    // NEW METHOD
    removeAllItemsFromProduct(product: Product) {
      const index = this._products.items.findIndex(item => item.id === product.id);

      if (index !== -1) {
        this._products.items.splice(index, 1);
        this._products.total = this.calculateTotal(this._products.items);
        this._cartState.next(this._products);
      }
    }

    removeProduct(product: Product, shouldRemoveAll = false) {
      if (shouldRemoveAll) {
        this._products.items = [];
        this._products.total = 0;
      } else {
        const index = this._products.items.findIndex(item => item.id === product.id);

        if (index !== -1) {
          let item = this._products.items[index];

          if (item.amount > 1) {
            item.amount -= 1;
            this._products.items[index] = this.calculateSubtotal(item);
          } else {
            this._products.items.splice(index, 1);
          }

          this._products.total = this.calculateTotal(this._products.items);
        }
      }

      this._cartState.next(this._products);
    }

    //HELPER METHODS

    protected updateCartState(products: CartProducts) {
        this._products = products;
        this._cartState.next(products);
    }

    protected calculateTotal(items: CartItem[]): number {
        return items.reduce((total, item) => total += item.subtotal, 0);
    }

    protected calculateSubtotal(item: CartItem): CartItem {
        item.subtotal = item.product.price * item.amount;
        return item;
    }

    protected getProducts() {
        return this._products;
    }

    getItems() {
        return this.getProducts().items;
    }

    getItem(id: number) {
        return this.getProducts().items.find(item => item.id === id);
    }

    getTotal() {
        return this.getProducts().total;
    }

    getCartUpdates() {
        return this._cartState.pipe(map(() => this.getItems()));
    }

    getItemUpdates(id: number) {
        return this._cartState.pipe(map(() => this.getItem(id)));
    }

    getTotalUpdates() {
        return this._cartState.pipe(map((s) => s.total));
    }
}
